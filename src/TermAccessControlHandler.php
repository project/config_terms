<?php

namespace Drupal\config_terms;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the config_terms term entity type.
 *
 * @see \Drupal\config_terms\Entity\Term
 */
class TermAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $is_admin = AccessResult::allowedIfHasPermission($account, 'administer config terms');
    if ($is_admin->isAllowed()) {
      return $is_admin;
    }

    if ($operation === 'view') {
      return AccessResult::allowedIfHasPermission($account, 'access content');
    }

    /** @var \Drupal\config_terms\Entity\TermInterface $entity */
    if ($operation === 'update') {
      return AccessResult::allowedIfHasPermission($account, 'edit terms in ' . $entity->getVid());
    }

    if ($operation === 'delete') {
      return AccessResult::allowedIfHasPermission($account, 'delete terms in ' . $entity->getVid());
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer config terms');
  }

}
