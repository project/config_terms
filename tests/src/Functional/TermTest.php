<?php

namespace Drupal\Tests\config_terms\Functional;

use Drupal\config_terms\Entity\Term;
use Drupal\config_terms\Entity\TermInterface;
use Drupal\config_terms\Entity\Vocab;
use Drupal\Tests\BrowserTestBase;

/**
 * Basic web tests for Config Terms functionality.
 *
 * @group config_terms
 */
class TermTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_terms'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The config terms access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $accessControlHandler;

  /**
   * The web-assert option for this test.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $webAssert;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->accessControlHandler = $this->container
      ->get('entity_type.manager')
      ->getAccessControlHandler('config_terms_term');

    $this->webAssert = $this->assertSession();
  }

  /**
   * Tests the access handler for config terms.
   */
  public function testTermAccessHandler(): void {
    $vocab = Vocab::create([
      'label' => 'Test Vocab',
      'id' => 'test_vocab',
      'weight' => 0,
    ]);
    $vocab->save();

    $term = Term::create([
      'id' => 'test_term',
      'vid' => $vocab->id(),
      'label' => 'Test Term',
      'weight' => 0,
    ]);
    $term->save();

    $alt_vocab = Vocab::create([
      'label' => 'Alternative Test Vocab',
      'id' => 'alt_test_vocab',
      'weight' => 1,
    ]);
    $alt_vocab->save();

    $alt_term = Term::create([
      'id' => 'alt_test_term',
      'vid' => $alt_vocab->id(),
      'label' => 'Alternative Test Term',
      'weight' => 0,
    ]);
    $alt_term->save();

    $this->drupalLogin($this->drupalCreateUser(['administer config terms']));
    $this->assertTermAccess($term, [
      'add' => TRUE,
      'update' => TRUE,
      'delete' => TRUE,
    ]);
    $this->assertTermAccess($alt_term, [
      'add' => TRUE,
      'update' => TRUE,
      'delete' => TRUE,
    ]);

    $this->drupalLogin($this->drupalCreateUser(['access content']));
    $basic_user_expects = ['add' => FALSE, 'update' => FALSE, 'delete' => FALSE];
    $this->assertTermAccess($term, $basic_user_expects);
    $this->assertTermAccess($alt_term, $basic_user_expects);

    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'edit terms in test_vocab',
      'delete terms in test_vocab',
    ]));
    $this->assertTermAccess($term, [
      'add' => FALSE,
      'update' => TRUE,
      'delete' => TRUE,
    ]);
    $this->assertTermAccess($alt_term, [
      'add' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
    ]);

    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'edit terms in alt_test_vocab',
    ]));
    $this->assertTermAccess($term, [
      'add' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
    ]);
    $this->assertTermAccess($alt_term, [
      'add' => FALSE,
      'update' => TRUE,
      'delete' => FALSE,
    ]);

    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'delete terms in alt_test_vocab',
    ]));
    $this->assertTermAccess($term, [
      'add' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
    ]);
    $this->assertTermAccess($alt_term, [
      'add' => FALSE,
      'update' => FALSE,
      'delete' => TRUE,
    ]);
  }

  /**
   * Asserts that the given match the expected results for a given term.
   *
   * @param \Drupal\config_terms\Entity\TermInterface $test_term
   *   The term to test.
   * @param bool[] $expected_access
   *   The expected access results for the given operations, which is the key:
   *   - add: Whether the user can add a new term of the same type.
   *   - update: Wheher the user can edit the given term.
   *   - delete: Whether the user can delete the given term.
   */
  protected function assertTermAccess(TermInterface $test_term, array $expected_access): void {
    // View access is always true, and there's no entity view route by default.
    $this->assertTrue($test_term->access('view', NULL, TRUE)->isAllowed());

    $this->drupalGet("/admin/structure/config-terms/{$test_term->getVid()}/overview");
    $this->webAssert->statusCodeEquals($expected_access['add'] ? 200 : 403);
    $this->assertSame(
      $expected_access['add'],
      $this->accessControlHandler->createAccess()
    );

    $this->drupalGet("/admin/structure/config-terms/{$test_term->getVid()}/add");
    $this->webAssert->statusCodeEquals($expected_access['add'] ? 200 : 403);
    $this->assertSame(
      $expected_access['add'],
      $this->accessControlHandler->createAccess()
    );

    $this->drupalGet("/config-terms/config-term/{$test_term->id()}/edit");
    $this->webAssert->statusCodeEquals($expected_access['update'] ? 200 : 403);
    $this->assertSame(
      $expected_access['update'],
      $test_term->access('update', NULL, TRUE)->isAllowed()
    );

    $this->drupalGet("/config-terms/term/{$test_term->id()}/delete");
    $this->webAssert->statusCodeEquals($expected_access['delete'] ? 200 : 403);
    $this->assertSame(
      $expected_access['delete'],
      $test_term->access('delete', NULL, TRUE)->isAllowed()
    );
  }

}
